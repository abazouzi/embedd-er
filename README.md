
# EMBEDD-ER : EMBEDDing Educational Resources using Linked Open Data

This repository is the code for the article about EMBEDD-ER. 

To install the necessary packages :

```pip install requirements.txt```

OR

You can use the instructions found in the ```start.sh``` file.

The repository is structured as follows :
* ```Classification.ipynb``` : demonstration of binary classification for EMBEDD-ER and three other baselines (TF-IDF, Doc2Vec, and BERT).
* ```Generation.ipynb``` : demonstration of how the embeddings are generated for EMBEDD-ER.
* ```Heatmaps.ipynb``` : demonstration of the similarity between educational resources using l2-norm and cosine similarity with EMBEDd-ER and BERT.
* ```methods``` : contains files that generate the embeddings used.
* ```classification``` : contains files used for classification.
* ```preprocessing``` : contains files used in preprocessing the text to use it with the embedding methods.
* ```similarity``` : contains the siilarity measures used.
* ```data``` : contains the data files used in this work (Open Yale Courseware).


To be able to run the code, you need to download the data as well as the Wikipedia2Vec model. They should be put in the following folders in the root of the directory :
* ```models``` : should contain the Wikipedia2Vec model which can be downloaded from https://wikipedia2vec.github.io/wikipedia2vec/pretrained/ 

