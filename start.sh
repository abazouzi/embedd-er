python3 -m pip install virtualenv
python3 -m venv embedder
source embedder/bin/activate
python3 -m pip install -r requirements.txt