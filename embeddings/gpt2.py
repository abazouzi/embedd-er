from simpletransformers.language_representation import RepresentationModel


def gpt2_embeddings(sentences):
    model_gpt2 = RepresentationModel(
        model_type = "gpt2",
        model_name = "gpt2-large",
        use_cuda = False
    )
    sentences_vector_gpt2 = list(model_gpt2.encode_sentences(sentences, combine_strategy = "mean"))
    
    return sentences_vector_gpt2