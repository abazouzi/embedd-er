from simpletransformers.language_representation import RepresentationModel


def bert_embeddings(sentences):
    model_bert = RepresentationModel(
        model_type = "bert",
        model_name = "bert-base-uncased",
        use_cuda = False
    )
    sentences_vector_bert = list(model_bert.encode_sentences(sentences, combine_strategy = "mean"))
    
    return sentences_vector_bert