from simpletransformers.language_representation import RepresentationModel


def roberta_embeddings(sentences):
    model_roberta = RepresentationModel(
        model_type = "roberta",
        model_name = "roberta-base",
        use_cuda = False
    )
    sentences_vector_roberta = list(model_roberta.encode_sentences(sentences, combine_strategy = "mean"))
    
    return sentences_vector_roberta